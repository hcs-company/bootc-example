current_dir := $(shell basename $(shell pwd))
.PHONY: container qcow2

container:
	podman build . -t registry.gitlab.com/hcs-company/bootc-example/$(current_dir):latest
	podman push registry.gitlab.com/hcs-company/bootc-example/$(current_dir):latest

qcow2:
	sudo podman run --rm -it --privileged -v .:/output --pull newer registry.redhat.io/rhel9/bootc-image-builder:9.4 --type qcow2 registry.gitlab.com/hcs-company/bootc-example/$(current_dir):latest

cloud-init-vm:
	sudo virt-install --name $(current_dir) --memory 2048 --vcpus 2 --disk qcow2/disk.qcow2 --import --os-variant rhel9.4 --noautoconsole --cloud-init=disable=on,user-data=cloud-init/user-data,meta-data=cloud-init/meta-data

ks-vm:
	sudo virt-install --name $(current_dir) --memory 4096 --vcpus 2 --disk size=20 --boot uefi --pxe --os-variant rhel9.4 --autoconsole=text
